# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Alternative for Paradox Launcher"
HOMEPAGE="https://github.com/shusaura85/notparadoxlauncher"

if [[ ${PV} == "9999" ]]; then
	SRC_URI="https://github.com/shusaura85/notparadoxlauncher/archive/master.tar.gz"
else
	SRC_URI="https://github.com/shusaura85/notparadoxlauncher/archive/v${PV}.tar.gz"
	KEYWORDS="~amd64"
fi

LICENSE="MIT"
SLOT="0"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
