# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit pam systemd

DESCRIPTION="TUI (ncurses-like) display manager"
HOMEPAGE="https://github.com/cylgom/ly"

SUBMODULES=( 'argoat' 'testoasterror' 'configator' 'ctypes' 'dragonfail' 'termbox_next' )

COMMON_URI="
		https://github.com/cylgom/argoat/archive/master.tar.gz -> argoat.tar.gz
		https://github.com/cylgom/testoasterror/archive/master.tar.gz -> testoasterror.tar.gz
		https://github.com/cylgom/configator/archive/master.tar.gz -> configator.tar.gz
		https://github.com/cylgom/ctypes/archive/master.tar.gz -> ctypes.tar.gz
		https://github.com/cylgom/dragonfail/archive/master.tar.gz -> dragonfail.tar.gz
		https://github.com/cylgom/termbox_next/archive/master.tar.gz -> termbox_next.tar.gz
"

if [[ ${PV} == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/cylgom/ly.git"
	SRC_URI="${COMMON_URI}"
else
	SRC_URI="
		${COMMON_URI}
		https://github.com/cylgom/ly/archive/v${PV}.tar.gz -> ${PF}.tar.gz
	"
	KEYWORDS="~amd64"
fi

LICENSE="WTFPL-2"
SLOT="0"
IUSE="systemd"

DEPEND="
	sys-libs/pam
	sys-apps/util-linux
	sys-libs/ncurses
	x11-apps/xauth
	x11-base/xorg-server
	x11-libs/libxcb
"
RDEPEND="
	${DEPEND}
	systemd? ( sys-apps/systemd )
"

src_unpack() {
	default
	for i in "${SUBMODULES[@]}"; do
		unpack ${i}.tar.gz
	done
}

src_prepare() {
	for i in "${SUBMODULES[@]}"; do
		if [[ ${i} == "testoasterror" ]]; then
			rmdir "${S}/sub/argoat/sub/${i}"
			mv "${WORKDIR}/${i}-master" "${S}/sub/argoat/sub/${i}"
		else
			rmdir "${S}/sub/${i}"
			mv "${WORKDIR}/${i}-master" "${S}/sub/${i}"
		fi
	done
	default
}

src_install() {
	dobin bin/ly
	insinto /etc/${PN}
	doins res/{config.ini,{w,x}setup.sh}
	insinto /etc/${PN}/lang
	doins res/lang/*
	dopamd res/pam.d/ly
	if use systemd; then
		systemd_dounit res/ly.service
	fi
}
