# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit qmake-utils

DESCRIPTION="placeholder"
HOMEPAGE="http://users.telenet.be/Jan.Van.hijfte/qtforfpc/fpcqt4.html"
SRC_URI="http://users.telenet.be/Jan.Van.hijfte/qtforfpc/V2.6/splitbuild-qt5pas-V2.6Alpha_Qt5.1.1.tar.gz"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/splitbuild-qt5pas-V2.6Alpha_Qt5.1.1"

src_configure() {
	eqmake5
}
