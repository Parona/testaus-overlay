# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit optfeature toolchain-funcs

MY_PV=$(ver_rs 1- _ $(ver_cut 2-3))

DESCRIPTION="A single-player fantasy game"
HOMEPAGE="http://www.bay12games.com/dwarves/"
SRC_URI="amd64? ( http://www.bay12games.com/dwarves/df_"${MY_PV}"_linux.tar.bz2 )
	x86? ( http://www.bay12games.com/dwarves/df_"${MY_PV}"_linux32.tar.bz2 )"

LICENSE="free-noncomm BSD BitstreamVera"
SLOT="0"
KEYWORDS="~amd64"
DEPEND="
	media-libs/glew:0
	media-libs/libsdl[X,joystick,video]
	media-libs/sdl-image[png]
	media-libs/sdl-ttf
	sys-libs/zlib
	virtual/glu
	x11-libs/gtk+:2
	media-libs/openal
	media-libs/libsndfile
"

RDEPEND="
	${DEPEND}
	sys-libs/ncurses-compat:5[unicode]
"

S=""${WORKDIR}"/df_linux"
GAMES_DIR="/opt/"${PN}""
QA_PREBUILT="${GAMES_DIR#/}/libs/Dwarf_Fortress"
RESTRICT="strip"

src_prepare() {
	find libs/ ! -name 'Dwarf_Fortress' ! -name 'libgraphics.so' -type f -exec rm -f {} + || die
	sed -e "s:^GAMES_DIR=\".*\"$:GAMES_DIR=\""${GAMES_DIR}"\":" "${FILESDIR}/dwarf-fortress" > dwarf-fortress || die
	default
}

src_install() {
	# install data-files and libs
	insinto "${GAMES_DIR}"
	doins -r raw data libs
	# install our wrapper
	dobin dwarf-fortress
	dodoc README.linux *.txt
	fperms 755 "${GAMES_DIR}"/libs/Dwarf_Fortress
}

pkg_postinst() {
	elog "System-wide Dwarf Fortress has been installed to ${GAMES_DIR}. This is"
	elog "symlinked to ~/.dwarf-fortress when dwarf-fortress is run."
	elog "For more information on what exactly is replaced, see /usr/bin/dwarf-fortress."
	elog "Note: This means that the primary entry point is /usr/bin/dwarf-fortress."
	elog "Do not run ${GAMES_DIR}/libs/Dwarf_Fortress."

	optfeature "for [PRINT_MODE:TEXT]" sys-libs/ncurses[unicode]
	optfeature "audio output" media-libs/openal media-libs/libsndfile
	optfeature "OpenGL PRINT_MODE settings" media-libs/libsdl[opengl]
}
