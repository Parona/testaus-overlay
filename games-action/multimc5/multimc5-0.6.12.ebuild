# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake desktop java-pkg-2 xdg

DESCRIPTION="An opensource Qt5 based Minecraft launcher"
HOMEPAGE="https://multimc.org/"

if [[ "${PV}" == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/MultiMC/MultiMC5.git"
	EGIT_SUBMODULES=( '*' )
else
	# MultiMC5 uses their own for of libnbtplusplus and upstream libnbtplusplus is quiet and unused by any other project.
	LIBNBTPLUSPLUS_COMMIT="dc72a20b7efd304d12af2025223fad07b4b78464"
	# MultiMC5 uses their own fork of quazip, therefore can't use main tree version.
	QUAZIP_COMMIT="3691d57d3af13f49b2be2b62accddefee3c26b9c"
	SRC_URI="
		https://github.com/MultiMC/MultiMC5/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
		https://github.com/MultiMC/libnbtplusplus/archive/${LIBNBTPLUSPLUS_COMMIT}.tar.gz -> ${PN}-libnbtplusplus-${LIBNBTPLUSPLUS_COMMIT}.tar.gz
		https://github.com/MultiMC/quazip/archive/${QUAZIP_COMMIT}.tar.gz -> ${PN}-quazip-${QUAZIP_COMMIT}.tar.gz
	"
	S="${WORKDIR}/MultiMC5-${PV}"
	KEYWORDS="~amd64"
fi

# Main project Apache-2.0
# Bundled LocalPeer BSD (3-clause)
# Bundled classparser Apache-2.0
# Bundled ganalytics BSD (3-clause)
# Bundled hoedown MIT
# Bundled iconfix LGPL-2.1
# Bundled javacheck: no license and too trivial to be under enforceable license
# Bundled launcher Apache-2.0
# Bundled libnbtplusplus LGPL-3+
# Bundled optional-bare BSL-1.0
# Bundled quazip LGPL-2.1-with-linking-exception
# Bundled rainbow LGPL-2.1+
# Bundled systeminfo MIT
# Bundled xz-embedded public-domain

LICENSE="Apache-2.0 BSD LGPL-2.1 LGPL-2.1+ LGPL-2.1-with-linking-exception LGPL-3+ MIT public-domain"
SLOT="0"
# Tests depend on cxxtest which is unmaintained and depends on python2
RESTRICT="test"

COMMON_DEPEND="
	dev-qt/qtconcurrent
	dev-qt/qtcore
	dev-qt/qtnetwork
	dev-qt/qttest
	dev-qt/qtwidgets
	dev-qt/qtxml
	sys-libs/zlib
"
RDEPEND="
	${COMMON_DEPEND}
	virtual/jre
	virtual/opengl
"
DEPEND="${COMMON_DEPEND}"
BDEPEND="virtual/jdk"

PATCHES=(
	"${FILESDIR}/GCC11-fixes.patch"
)

src_prepare() {
	if [[ "${PV}" == "9999" ]]; then
		git-r3_src_prepare
	else
		rmdir libraries/{libnbtplusplus,quazip} || die
		mv "${WORKDIR}/libnbtplusplus-${LIBNBTPLUSPLUS_COMMIT}" "${S}/libraries/libnbtplusplus" || die
		mv "${WORKDIR}/quazip-${QUAZIP_COMMIT}" "${S}/libraries/quazip" || die
	fi
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DCMAKE_INSTALL_PREFIX="${EPREFIX}/usr"
		-DMultiMC_ANALYTICS_ID=""
		-DMultiMC_BUILD_PLATFORM="Gentoo"
		-DMultiMC_LAYOUT="lin-system"
	)
	cmake_src_configure
}

src_compile() {
	cmake_src_compile
}

src_install() {
	cmake_src_install
	domenu application/package/linux/multimc.desktop
	doicon -s scalable application/resources/multimc/scalable/multimc.svg
}
