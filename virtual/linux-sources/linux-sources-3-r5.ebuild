# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Virtual for Linux kernel sources"
SLOT="0"
KEYWORDS="~amd64"
IUSE="firmware"

RDEPEND="
	firmware? ( sys-kernel/linux-firmware )
	|| (
		sys-kernel/bliss-kernel-bin
		sys-kernel/ck-sources
		sys-kernel/gentoo-kernel
		sys-kernel/gentoo-kernel-bin
		sys-kernel/gentoo-sources
		sys-kernel/git-sources
		sys-kernel/liquorix-sources
		sys-kernel/mips-sources
		sys-kernel/pf-sources
		sys-kernel/rt-sources
		sys-kernel/raspberrypi-sources
		sys-kernel/vanilla-kernel
		sys-kernel/vanilla-sources
		sys-kernel/xanmod-sources
		sys-kernel/zen-sources
	)"
