# Testaus-overlay

A personal overlay with which i tinker depending on my mood.

| Ebuild | Why | Status |
| :----- | --- | -----: |
| dev-libs/qt5pas | For games-util/notparadoxlauncher | ? |
| dev-libs/tweeny | No sure | ? |
| games-roguelike/dwarf-fortress | Bump + wanted to improve | Bumped but still not happy |
| games-util/notparadoxlauncher | For personal use | ? |
| media-video/ffmpeg | Modify lto logic to work with my setup | Works |
| virtual/linux-sources | Didn't want to keep gentoo-sources on my system | Works |
| x11-misc/ly | Wanted to use it | Builds but daemon dies mysteriosly |
